package no.petter.kalender

import no.petter.kalender.solutions.aoc._
import no.petter.kalender.solutions.knowit._
import zio.{ExitCode, URIO, ZIO, console}

object Main extends zio.App {

  private val currentTask: ZIO[zio.ZEnv, Throwable, Any] = knowit11.solution

  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] =
    currentTask
      .timed
      .tap{case (time, res) => console.putStrLn(res.toString) *> console.putStrLn((time.getNano / 1e6).toString)}
      .exitCode


}
