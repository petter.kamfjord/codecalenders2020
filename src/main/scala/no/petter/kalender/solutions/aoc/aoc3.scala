package no.petter.kalender.solutions.aoc

import zio.Task

import scala.annotation.tailrec
import scala.io.Source

object aoc3 {

  private val path = Source.fromResource("aoc3.txt").getLines.toList.filter(_.nonEmpty)
  private val horizontalLength = path.head.length

  @tailrec
  def recAdaptable(paths: List[String], currX: Int, sumTrees: Int, xMovement: Int, yMovement: Int): Int = paths match {
    case Nil => sumTrees
    case y :: tail =>
      val x = (currX + xMovement) % horizontalLength
      y.charAt(x) match {
        case '.' => recAdaptable(tail.drop(yMovement), x, sumTrees, xMovement, yMovement)
        case '#' => recAdaptable(tail.drop(yMovement), x, sumTrees+1, xMovement, yMovement)
      }
  }

  def solution: Task[Int] = Task.succeed(recAdaptable(path.tail, 0, 0, 3, 0))

  case class Movement(x: Int, y: Int)
  def solution2: Task[BigInt] = {
    val movements = List(Movement(1,1), Movement(3,1), Movement(5,1), Movement(7,1), Movement(1,2))
    Task.foreachPar(movements)(m => Task.succeed(recAdaptable(path.drop(m.y), 0, 0, m.x, m.y-1))).map(_.map(_.toLong).product)
  }





}
