package no.petter.kalender.solutions.aoc


import zio.Task

import scala.io.Source

object aoc2 {




  case class Password(min: Int, max: Int, char: Char, password: String)

  private val pwRegex = "^(\\d+)-(\\d+) (.*): (.+)$".r
  private val passwords = Source.fromResource("aoc2.txt").getLines.toList.flatMap{
    case pwRegex(min, max, char, password) => Some(Password(min.toInt, max.toInt, char.charAt(0), password))
    case _ => None
  }


  def solution: Task[Int] = Task.succeed(
    passwords.count{pw =>
      val count = pw.password.count(_ == pw.char)
      pw.min <= count && count <= pw.max
    }
  )


  def solution2: Task[Int] = Task.succeed{
    passwords.count{pw =>
      val charMin = pw.password.charAt(pw.min-1)
      val charMax = pw.password.charAt(pw.max-1)

      val pred1 = charMin == pw.char && charMax != pw.char
      val pred2 = charMax == pw.char && charMin != pw.char

      pred1 || pred2
    }
  }


}
