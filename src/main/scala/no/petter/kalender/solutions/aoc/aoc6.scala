package no.petter.kalender.solutions.aoc

import zio.Task

import scala.annotation.tailrec
import scala.io.Source

object aoc6 {


  private val answers = Source.fromResource("aoc6.txt").getLines.toList

  @tailrec
  private def getAnswers(list: List[String], currAcc: List[String], acc: List[List[String]]): List[List[String]] = list match {
    case Nil => currAcc :: acc
    case h :: tail => h match {
      case _ if h.trim.isEmpty => getAnswers(tail, Nil, currAcc :: acc)
      case _ => getAnswers(tail, h :: currAcc, acc)
    }
  }

  def solution1: Task[Int] = Task.succeed(getAnswers(answers, Nil, Nil).map(_.flatten.distinct.size).sum)


  def solution: Task[Int] = Task.succeed(
    getAnswers(answers, Nil, Nil).map{answers =>
      val allChars = answers.flatten.distinct
      allChars.count(c => answers.forall(_.contains(c)))
    }.sum
  )

}
