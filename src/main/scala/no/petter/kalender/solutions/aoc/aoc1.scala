package no.petter.kalender.solutions.aoc

import zio.Task

import scala.io.Source

object aoc1 {

  private val numbers = Source.fromResource("aoc1.txt").getLines.toList.map(_.toInt)
  private val target = 2020

  def solN(n: Int): Task[Option[Int]] = Task.succeed(numbers.combinations(n).collectFirst{case list if list.sum == target => list.product})

  def solution: Task[(Option[Int], Option[Int])] = solN(2) <&> solN(3)


}
