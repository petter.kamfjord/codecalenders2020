package no.petter.kalender.solutions.aoc

import zio.Task

object aoc7 {

  case class Contains(n: Int, color: String)
  case class Bag(color: String, canContain: List[Contains])


  private val GOLDEN = "shiny gold"

  def parseBags(l: List[String]): Contains = l match {
    case n :: c1 :: c2 :: _ => Contains(n.toInt, s"$c1 $c2")
  }

  def parseInput(input: String): Bag = input.split(' ').toList match {
    case c1 :: c2 :: _ :: "contain" :: "no" :: "other" :: "bags." :: Nil => Bag(s"$c1 $c2", Nil)
    case c1 :: c2 :: _ :: "contain" :: bags => Bag(s"$c1 $c2", bags.mkString(" ").split(',').toList.map(l => parseBags(l.split(' ').toList.filterNot(_.isEmpty))))
  }


  private val bags = scala.io.Source.fromResource("aoc7.txt").getLines.toList.map(parseInput)


  def solve(bag: Bag): Boolean = {
    if(bag.color == GOLDEN)
      true
    else
      bag.canContain match {
        case Nil => false
        case _ => bag.canContain.exists(e => solve(bags.find(_.color == e.color).get))
      }
  }


  def solution1: Task[Int] = Task.succeed(bags.filterNot(_.color == GOLDEN).count(e => solve(e)))


}
