package no.petter.kalender.solutions.aoc

import zio.Task

import scala.annotation.tailrec
import scala.io.Source

object aoc4 {


  private val passportsRaw = Source.fromResource("aoc4.txt").getLines.toList


  val requiredFields = List("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")


  @tailrec
  private def getPassports(list: List[String], currAcc: List[String], acc: List[List[String]]): List[List[String]] = list match {
    case Nil => currAcc :: acc
    case h :: tail => h match {
      case _ if h.trim.isEmpty => getPassports(tail, Nil, currAcc :: acc)
      case _ => getPassports(tail, h :: currAcc, acc)
    }
  }

  private def validateYear(value: Int, min: Int, max: Int): Boolean =
    value >= min && value <= max

  private def validateHairColor(value: String): Boolean = {
    val validChars = "0123456789abcdef".toList
    value.length == 6 && value.forall(validChars.contains)
  }

  private def validateEyeColor(value: String): Boolean = {
    val validColors = List("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
    validColors.contains(value)
  }

  private def validateHeight(rawValue: String): Boolean = {
    rawValue.splitAt(rawValue.length - 2) match {
      case (value, "cm") if value.toInt >= 150 && value.toInt <= 193 => true
      case (value, "in") if value.toInt >= 59 && value.toInt <= 76 => true
      case _ => false
    }

  }

  def validateKeyValue(key: String, value: String): Boolean = key match {
    case "byr" => value.forall(_.isDigit) && validateYear(value.toInt,1920, 2002)
    case "iyr" => value.forall(_.isDigit) && validateYear(value.toInt, 2010, 2020)
    case "eyr" => value.forall(_.isDigit) && validateYear(value.toInt, 2020, 2030)
    case "hcl" => value.startsWith("#") && validateHairColor(value.drop(1))
    case "ecl" => validateEyeColor(value)
    case "hgt" => validateHeight(value)
    case "pid" => value.forall(_.isDigit) && value.length == 9
    case "cid" => true
  }

  def solution: Task[Int] = Task.succeed{
    val passports = getPassports(passportsRaw, Nil, Nil).map(_.mkString)
    passports.count(str => requiredFields.forall(str.containsSlice(_)))
  }

  def solution2: Task[Int] = Task.succeed{
    val passports = getPassports(passportsRaw, Nil, Nil).map(_.mkString(";").replace(' ', ';'))
    passports.count{passportStr =>
      requiredFields.forall(passportStr.containsSlice(_)) && passportStr.split(";").forall { keyValue =>
        val keyValueArr = keyValue.split(":")
        validateKeyValue(keyValueArr(0), keyValueArr(1))
      }
    }

  }

}
