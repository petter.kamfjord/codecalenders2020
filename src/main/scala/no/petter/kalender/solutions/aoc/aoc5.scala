package no.petter.kalender.solutions.aoc

import zio.Task

import scala.annotation.tailrec
import scala.io.Source

object aoc5 {

  private val passes = Source.fromResource("aoc5.txt").getLines.toList

  @tailrec
  private def findRow(directions: List[Char], min: Int, max: Int): Int = directions match {
    case ('F' | 'L') :: Nil => min
    case ('B' | 'R') :: Nil => max
    case h :: tail => h match {
      case 'F' | 'L' => findRow(tail, min, (min+max)/2)
      case 'B' | 'R' => findRow(tail, ((max+min)/2)+1, max)
    }
  }


  def solve(row: List[Char]): Int = {
    val (fb, lr) = row.splitAt(7)
    (findRow(fb, 0, 127) * 8) + findRow(lr, 0, 7)
  }


  def solution: Task[List[Int]] = Task.succeed {
    val takenPasses = passes.map(p => solve(p.toList))
    (0 to 842).toList.filter(i => !takenPasses.contains(i) && takenPasses.contains(i-1) && takenPasses.contains(i+1))
  }


}
