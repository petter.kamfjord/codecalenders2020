package no.petter.kalender.solutions.knowit

import zio.Task

import scala.io.Source

object knowit5 {

  private val directions = Source.fromResource("knowit5.txt").toList

  case class Point(x: Int, y: Int)

  @scala.annotation.tailrec
  def getPoints(dir: List[Char], currX: Int, currY: Int, acc: List[Point]): List[Point] = dir match {
    case Nil => acc
    case h :: tail => h match {
      case 'H' => getPoints(tail, currX+1, currY, Point(currX, currY) :: acc)
      case 'V' => getPoints(tail, currX-1, currY, Point(currX, currY) :: acc)
      case 'O' => getPoints(tail, currX, currY+1, Point(currX, currY) :: acc)
      case 'N' => getPoints(tail, currX, currY-1, Point(currX, currY) :: acc)
    }
  }

  @scala.annotation.tailrec
  def calculateArea(list: List[Point], offset: List[Point], head: Point, acc: Int): Int = (list, offset) match {
    case (h1 :: Nil, Nil) =>
      val x = h1.x * head.y
      val y = h1.y * head.x
      (acc + (x-y)) / 2
    case (h1 :: tail1, h2 :: tail2) =>
      val x = h1.x * h2.y
      val y = h1.y * h2.x
      calculateArea(tail1, tail2, head, acc + (x-y))
  }

  def solution: Task[Int] = Task.succeed{
    val coordinates = getPoints(directions, 0, 0, Nil).reverse

    val head :: offset = coordinates

    calculateArea(coordinates, offset, head, 0)
  }

}
