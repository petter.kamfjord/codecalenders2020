package no.petter.kalender.solutions.knowit

import zio.Task

import scala.io.Source

object knowit6 {


  private val packages = Source.fromResource("knowit6.txt").getLines.flatMap(_.split(',').toList).toList.map(_.toInt)

  @scala.annotation.tailrec
  def recursion(l: List[Int], currSum: Int, acc: Int, numElves: Int): Int = l match {
    case Nil => acc
    case h :: tail =>
      val newSum = currSum + h
      newSum % numElves match {
        case 0 => recursion(tail, 0, acc+(newSum/numElves), numElves)
        case _ => recursion(tail, newSum, acc, numElves)
      }
  }

  def solution: Task[Int] = Task.succeed(recursion(packages, 0, 0, 127))

}
