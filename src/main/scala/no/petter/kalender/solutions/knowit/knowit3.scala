package no.petter.kalender.solutions.knowit

import zio.Task

import scala.annotation.tailrec
import scala.io.Source

object knowit3 {


  val words = List("nisseverksted",
    "pepperkake",
    "adventskalender",
    "klementin",
    "krampus",
    "juletre",
    "julestjerne",
    "gløggkos",
    "marsipangris",
    "mandel",
    "sledespor",
    "nordpolen",
    "nellik",
    "pinnekjøtt",
    "svineribbe",
    "lutefisk",
    "medisterkake",
    "grevinne",
    "hovmester",
    "sølvgutt",
    "jesusbarnet",
    "julestrømpe",
    "askepott",
    "rudolf",
    "akevitt")


  case class Point(x: Int, y: Int, char: Char)


  private val matrix = Source.fromResource("knowit3.txt").getLines.toList.filterNot(_.isEmpty)
  private val MAX = matrix.head.length - 1

  def solution: Task[String] = {
    val indexed = matrix.map(_.zipWithIndex.map(_.swap).toMap).zipWithIndex.map(_.swap).toMap

    @scala.annotation.tailrec
    def getDiagonal(xStep: Int => Int, yStep: Int => Int, x: Int, y: Int, acc: List[Char]): List[Char] = (x,y) match {
      case (_,_) if x > MAX || y > MAX || x < 0 || y < 0 => acc
      case (x,y) =>
        val char = indexed(y)(x)
        getDiagonal(xStep, yStep, xStep(x), yStep(y), char :: acc)
    }

    val horizontal = matrix.map(_.toList)
    val vertical = horizontal.transpose

    val range = (0 to MAX).toList

    val ops: List[(Int => Int, Int => Int)] = List(
      (_ + 1, _ + 1),
      (_ - 1, _ + 1),
      (_ - 1, _ - 1),
      (_ + 1, _ - 1),
    )

    val startPos: List[(Int, Int)] = for {
      x <- range
      y <- range
      if x == MAX || x == 0 || y == MAX || y == 0 //TODO..
    } yield (x,y)

    val allDiags = startPos.map { case (x, y) =>
      ops.map { case (xOp, yOp) =>
        getDiagonal(xOp, yOp, x, y, Nil)
      }
    }


    val all = horizontal :: vertical :: allDiags

    val res = Task.filterNotPar(words)(str =>
      Task.succeed(println("Checking " + str)) *>
        Task.succeed(all.exists(l => l.exists(_.containsSlice(str) || l.exists(_.containsSlice(str.reverse))))) <*
        Task.succeed(println("Done with " + str))

    )


    res.map(_.sorted.mkString(","))
  }

}
