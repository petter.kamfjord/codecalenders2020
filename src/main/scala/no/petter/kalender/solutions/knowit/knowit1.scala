package no.petter.kalender.solutions.knowit

import zio.Task

import scala.io.Source

object knowit1 {


  private val numbers = Source.fromResource("knowit1.txt").getLines.mkString.split(",").map(_.toInt)
  private val range = (1 to 100000).toList

  def solution: Task[List[Int]] = Task.succeed(range.diff(numbers))

}
