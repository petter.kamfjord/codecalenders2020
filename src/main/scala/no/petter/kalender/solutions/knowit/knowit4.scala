package no.petter.kalender.solutions.knowit

import zio.Task

import scala.annotation.tailrec
import scala.io.Source

object knowit4 {

  val SUGAR = 2
  val FLOUR = 3
  val MILK = 3

  private val shoppingList = Source.fromResource("knowit4.txt").getLines.mkString(",").split(",").toList

  private val regex = "([a-z]+): (\\d+)".r


  @tailrec
  def reg(list: List[String], eggs: Long, sugar: Long, flour: Long, milk: Long): Long = list match {
    case Nil => List(eggs, sugar / SUGAR, flour / FLOUR, milk / MILK).min
    case h :: tail =>
      h.trim match {
        case regex(tpe, amount) => tpe match {
          case "sukker" => reg(tail, eggs, sugar + amount.toLong, flour, milk)
          case "mel" => reg(tail, eggs, sugar, flour + amount.toLong, milk)
          case "melk" => reg(tail, eggs, sugar, flour, milk + amount.toLong)
          case "egg" => reg(tail, eggs + amount.toLong, sugar, flour, milk)
        }
        case _ => reg(tail, eggs, sugar, flour, milk)
      }
  }

  val solution: Task[Long] = Task.succeed(reg(shoppingList, 0L, 0L, 0L, 0L))

}
