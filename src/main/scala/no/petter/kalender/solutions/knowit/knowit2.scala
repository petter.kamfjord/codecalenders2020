package no.petter.kalender.solutions.knowit

import zio.Task

import scala.annotation.tailrec

object knowit2 {

  private val population = (0 until 5433000).toList


  private def isPrime(n: Int) = n match {
    case 2 => true
    case _ if n <= 1 => false
    case n => !(2 to (n/2)).exists(x => n % x == 0)
  }

  @tailrec
  def recursion(pop: List[Int], sum: Int, lastPrime: Int): Int = pop match {
    case Nil => sum
    case h :: tail =>
      val badNum = h.toString.contains('7')

      val nearestPrime = badNum match {
        case true => (h to lastPrime by -1).toList.find(isPrime)
        case false => None
      }

      val numToAdd = if(badNum) 0 else 1

      recursion(tail.drop(nearestPrime.getOrElse(0)), sum + numToAdd, nearestPrime.getOrElse(lastPrime))
  }

  def solution: Task[Int] = Task.succeed(recursion(population, 0, 1))

}
