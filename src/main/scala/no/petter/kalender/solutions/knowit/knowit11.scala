package no.petter.kalender.solutions.knowit

import zio.Task

import scala.annotation.tailrec

object knowit11 {

  private val target = "eamqia"
  private val words = scala.io.Source.fromResource("knowit11.txt").getLines.map(_.trim.toList)

  def addOneToChar(c: Char): Char = c match {
    case 'z' => 'a'
    case _ => (c + 1).toChar
  }

  def addChars(a: Char, b: Char): Char = {
    val newChar = ((a -'a') + (b - 'a')) % 26
    (newChar + 'a').toChar
  }



  @tailrec
  def recursion(str: List[Char], acc: List[List[Char]]): List[List[Char]] = str match {
    case _ :: Nil => (0 to acc.head.size).toList.map{n => acc.flatMap(_.drop(n).headOption)}
    case _ :: tail =>
      val last = acc.last
      val movedOnce = tail.map(addOneToChar)
      val newString = movedOnce.zip(last).map{case (a,b) => addChars(a,b)}
      recursion(newString, acc :+ newString)
  }





  def solution: Task[Option[String]] = {
    Task.succeed(words.collectFirst{ case word if recursion(word, List(word)).exists(_.containsSlice(target)) => word.mkString})
  }

}
