shellPrompt in ThisBuild := { state => Project.extract(state).currentRef.project + "> " }

val allScalacOptions = Seq(
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-encoding", "utf-8", // Specify character encoding used by source files.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-language:higherKinds", // Allow higher-kinded types
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-extra-implicit", // Warn when more than one implicit parameter section is defined.
  "-Ywarn-numeric-widen", // Warn when numerics are widened.
  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports", // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals", // Warn if a local definition is unused.
  "-Ywarn-unused:params", // Warn if a value parameter is unused.
  "-Ywarn-unused:patvars", // Warn if a variable bound in a pattern is unused.
  "-Ywarn-unused:privates", // Warn if a private member is unused.
  "-Ywarn-value-discard" // Warn when non-Unit expression results are unused.
)

val allResolvers = Seq(
  Resolver.sonatypeRepo("public"),
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots"),
)

lazy val root = (project in file("."))
  .settings(
    scalaVersion := "2.13.1",
    name := "KodeKalendre",
    parallelExecution := false,
    parallelExecution in Test := false,
    libraryDependencies := Seq(
      "dev.zio" %% "zio" % "1.0.3",
      "joda-time" % "joda-time" % "2.10.6",
    ),
    resolvers ++= allResolvers,
    scalacOptions ++= allScalacOptions
  )
